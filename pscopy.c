#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#define TAM 50
char buffer1[100];
long int contador=0;
int main(int argc , char **argv){
        memset(buffer1,0,100);
        umask(0);
        int fd1 =open(argv[1],O_RDONLY);
        if(fd1<0){
        perror("NO se pudo abrir \n");
}
        unsigned long leidos= read(fd1,buffer1,100);
        int fd2 =open(argv[2],O_WRONLY|O_CREAT|O_TRUNC,0666);
        if(fd2<0){
        perror("No se pudo crear el archivo \n");
}
while(leidos!=0){
	contador=contador+leidos;
        write(fd2,buffer1,strlen(buffer1));
	memset(buffer1,0,100);
	leidos=read(fd1,buffer1,100);
}
	close(fd1);
        close(fd2);
        printf("Los bytes copiados fueron %li\n",contador);
return 0;
}
